package io.simple.core;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: not
 * Date: 13-8-31
 * Time: 上午11:12
 * To change this template use File | Settings | File Templates.
 */
public interface SourceHandle {
    /**
     * @param source
     */
    void handle(File source);
}
