#JSIMPLE

```java
SourceSearcher.addHandle(ClassSourceHandler.getInstance());
ClassSourceHandler.addHandle(new ClassHandle() {
    @Override
    public void handle(Class clazz) {
        Bind bind = (Bind) clazz.getAnnotation(Bind.class);
        if(bind != null){
            System.out.println("bind :::::::: " + clazz.getName());
        }
    }
});
SourceSearcher.boot();
```